using Global;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CurrentGameScore : MonoBehaviour
    {
        [SerializeField] private Text coinText;
        private int _coins;
        
        private void Start()
        {
            GameEventsHandler.CoinGrabbed += UpdateCoinText;
            _coins = 0;
        }

        private void UpdateCoinText()
        {
            coinText.text = $"Fishes: {++_coins}";
        }


        private void OnDestroy()
        {
            var prevHighScore = PlayerPrefs.GetInt("HighScore", 0);
            
            if(_coins > prevHighScore)
                PlayerPrefs.SetInt("HighScore", _coins);
            
            GameEventsHandler.CoinGrabbed -= UpdateCoinText;
        }
    }
}
