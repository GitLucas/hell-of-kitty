using UnityAsync;
using UnityEngine;

namespace UI
{
    public class ArrowAnim : MonoBehaviour
    {
        private const int MAX = 3;

        public async void ArrowPressed()
        {
            var offset = 0;

            while (offset < MAX)
            {
                transform.position += Vector3.up;
                offset++;
                await Await.NextUpdate();
            }
            
            while (offset > 0)
            {
                transform.position += Vector3.down;
                offset--;
                await Await.NextUpdate();
            }

        }
    }
}
