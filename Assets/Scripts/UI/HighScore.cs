using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HighScore : MonoBehaviour
    {
        [SerializeField] private Text highScoreText;

        private void Start()
        {
            var max = PlayerPrefs.GetInt("HighScore", 0);
            highScoreText.text += $"{max}";
        }
    }
}
