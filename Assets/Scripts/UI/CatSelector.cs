using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CatSelector : MonoBehaviour
    {
        [SerializeField] private Sprite[] cats;
        [SerializeField] private Image portrait;
        [Space]
        [SerializeField] private ArrowAnim left;
        [SerializeField] private ArrowAnim right;

        private int _index;

        private void Start()
        {
            _index = PlayerPrefs.GetInt("CatIndex", 0);
            portrait.sprite = cats[_index];
        }

        private void Update()
        {
            if (!Input.GetButtonDown("Horizontal")) return;

            var add = (int) Mathf.Sign(Input.GetAxis("Horizontal"));
            
            if(add < 0)
                left.ArrowPressed();
            else
                right.ArrowPressed();

            _index = (cats.Length + _index + add) % cats.Length;
            portrait.sprite = cats[_index];
        }

        private void OnDestroy()
        {
            PlayerPrefs.SetInt("CatIndex", _index);
        }
    }
}
