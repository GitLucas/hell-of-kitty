#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Editor
{
    internal sealed class AssetImporter 
        : AssetPostprocessor
    {
        private const int PixelsPerUnit = 8;
        //private const int MaxTextureSize = 32;
        private const TextureImporterCompression TextureCompression = TextureImporterCompression.Uncompressed;

        private const FilterMode FilterMode = UnityEngine.FilterMode.Point;
        private const int Anisolevel = 1;
        private const int CompressionQuality = 100;

        // This event is raised when a texture asset is imported
        private void OnPreprocessTexture()
        {
            var importer = assetImporter as TextureImporter;

            if (!importer) return;
        
            //This is a 2d project, probably a Sprite
            importer.textureType = TextureImporterType.Sprite;
            importer.isReadable = false;
            importer.filterMode = FilterMode;
            importer.anisoLevel = Anisolevel;
            importer.spritePixelsPerUnit = PixelsPerUnit;
            importer.compressionQuality = CompressionQuality;
            importer.textureCompression = TextureCompression;
            importer.alphaIsTransparency = importer.DoesSourceTextureHaveAlpha();
            //importer.maxTextureSize = MaxTextureSize;
        }
    
    }
}

#endif