using System;
using System.Collections;
using System.Collections.Generic;
using Global;
using UnityAsync;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    [SerializeField] private List<BulletMovementType> toSpawn;
    [Tooltip("Delay between each bullet spawn, in seconds")]
    [SerializeField] private float delayBetweenSpawns;

    [SerializeField] private Animator myAnim;

    public event Action<BulletSpawner> SpawnComplete;

    private int _index;
    private static readonly int Explode = Animator.StringToHash("Explode");

    private void Awake()
    {
        myAnim = myAnim ? myAnim : GetComponent<Animator>();
    }

    public void Spawn()
    {
        myAnim.SetTrigger(Explode);
    }

    public async void ShootBullets()
    {
        _index = 0;

        while (_index < toSpawn.Count)
        {
            try
            {
                await Await.Seconds(delayBetweenSpawns);

                var bullet = BulletGetter.Instance.GetBullet(toSpawn[_index++]);

                bullet.transform.parent = transform;
                bullet.transform.localPosition = Vector3.zero;

                bullet.SetActive(true);
            }
            catch (NullReferenceException)
            {
                break;
            }
        }

        SpawnComplete?.Invoke(this);
    }
}
