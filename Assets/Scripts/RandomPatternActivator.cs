using UnityEngine;
using Random = UnityEngine.Random;

public class RandomPatternActivator : MonoBehaviour
{
    private PatternSpawner[] _spawners;


    private float _nextSpawnTime = 4f, _delta;

    private void Awake()
    {
        _spawners = FindObjectsOfType<PatternSpawner>();
    }

    private void Update()
    {
        _delta += Time.fixedDeltaTime;
        
        if (_delta < _nextSpawnTime) return;

        _delta = 0;
        int index;

        do
        {
            index = Random.Range(0, _spawners.Length);
        } while (_spawners[index].isActiveAndEnabled);

        _spawners[index].Activate();

    }
}
