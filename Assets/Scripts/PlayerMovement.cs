using Global;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float _speed;

    private void Awake()
    {
        _speed = Time.fixedDeltaTime * 20f;
    }

    private void FixedUpdate()
    {
        var v = Input.GetAxis("Vertical");
        var h = Input.GetAxis("Horizontal");

        var targetPos = transform.position;

        if (h != 0)
            targetPos += Vector3.right * (Mathf.Sign(h) * _speed);
        
        if (v != 0)
            targetPos += Vector3.up * (Mathf.Sign(v) * _speed);

        transform.position = ClampToMapLimits(targetPos);
    }
    
    private static Vector3 ClampToMapLimits(Vector3 v)
    {
        return new Vector3(
            Mathf.Clamp(v.x, Constants.LeftLimit, Constants.RightLimit),
            Mathf.Clamp(v.y, Constants.BottomLimit, Constants.TopLimit),
            Mathf.RoundToInt(v.z));
    }
}
