using System.Collections;
using System.Collections.Generic;
using Global;
using UnityEngine;
using Utils;

public class RandomCoinSpawner : MonoBehaviour
{
    private Pool<Coin> _coins;
    private float _nextSpawn, _delta;
    
    private void Start()
    {
        _coins = new Pool<Coin>(Resources.Load<Coin>("Coin"));
        _nextSpawn = Random.Range(4f, 9f);
    }

    
    private void Update()
    {
        _delta += Time.deltaTime;

        if (_delta < _nextSpawn) return;

        _delta = 0;
        _nextSpawn = Random.Range(4f, 9f);
        
        var coin = _coins.Get();
        
        var position = new Vector3(
            Random.Range(Constants.LeftLimit, Constants.RightLimit),
            Random.Range(Constants.BottomLimit, Constants.TopLimit),
            0
        );
        
        coin.transform.position = position;
        coin.gameObject.SetActive(true);
    }
}
