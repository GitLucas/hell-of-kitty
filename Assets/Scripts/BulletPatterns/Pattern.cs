using System;
using Global;
using UnityEngine;

namespace BulletPatterns
{
    public abstract class Pattern : MonoBehaviour
    {
        protected Vector3 StartingPosition;
        protected float Time;
    
        protected virtual void OnEnable()
        {
            StartingPosition = transform.localPosition;
            Time = 0;
            
            Invoke(nameof(DisableGameObject), 4f);
        }

        private void DisableGameObject()
        {
            Destroy(this);
            gameObject.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player")) return;

            GameEventsHandler.Instance.InvokeGameLost();
        }

        private void FixedUpdate()
        {
            BulletMovement();
        }

        protected abstract void BulletMovement();
    }
}
