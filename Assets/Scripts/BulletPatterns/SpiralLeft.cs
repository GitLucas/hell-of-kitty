using Global;
using UnityEngine;

namespace BulletPatterns
{
    public class SpiralLeft : Pattern
    {
        private float _radius;
    
        protected override void OnEnable()
        {
            base.OnEnable();
            _radius = 0;
        }

        protected override void BulletMovement()
        {
            _radius += Constants.BulletRotationRadiusIncrement;
            Time += Constants.BulletMovementSpeed;

            var x = Mathf.Sin(Time);
            var y = Mathf.Cos(Time);

            var target = StartingPosition;
            
            target += Vector3.left * x;
            target += Vector3.down * y;

            target *= _radius;

            transform.localPosition = target;
        }
    }
}
