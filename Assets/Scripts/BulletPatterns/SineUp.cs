using Global;
using UnityEngine;

namespace BulletPatterns
{
    public class SineUp : Pattern
    {
        protected override void BulletMovement()
        {
            Time += Constants.BulletMovementSpeed;
            var x = Mathf.Sin(Time);

            var target = StartingPosition;
            target += Vector3.up * Time;
            target += Vector3.right * x;

            transform.localPosition = target;
        }
    }
}
