using Global;
using UnityEngine;

namespace BulletPatterns
{
    public class Linear : Pattern
    {
        private int _x, _y;

        public void SetDirections(int x, int y)
        {
            _x = x;
            _y = y;
        }
        
        protected override void BulletMovement()
        {
            Time += Constants.BulletMovementSpeed;
            var target = StartingPosition;
            
            target += Vector3.right * (Time * _x);
            target += Vector3.up * (Time * _y);

            transform.localPosition = target;
        }
    }
}
