using Global;
using UnityEngine;

namespace BulletPatterns
{
    public class SpiralRight : Pattern
    {
        private float _radius;
    
        protected override void OnEnable()
        {
            base.OnEnable();
            _radius = 0;
        }

        protected override void BulletMovement()
        {
            Time += Constants.BulletMovementSpeed;
            _radius += Constants.BulletRotationRadiusIncrement;
            
            var x = Mathf.Sin(Time);
            var y = Mathf.Cos(Time);

            var target = StartingPosition;
            
            target += Vector3.right * x;
            target += Vector3.down * y;

            target *= _radius;

            transform.localPosition = target;
        }
    }
}
