using Global;
using UnityEngine;

namespace BulletPatterns
{
    public class SineRight : Pattern
    {
        protected override void BulletMovement()
        {
            Time += Constants.BulletMovementSpeed;
            var x = Mathf.Sin(Time);

            var target = StartingPosition;
            target += Vector3.right * Time;
            target += Vector3.up * x;

            transform.localPosition = target;
        }
    }
}
