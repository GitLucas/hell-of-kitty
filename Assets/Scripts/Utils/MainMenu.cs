using UnityEngine;

namespace Utils
{
    public class MainMenu : MonoBehaviour
    {
        public void GoButton() => CrossSceneObjectManager.Instance.GoToGame();
    }
}
