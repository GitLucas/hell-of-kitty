using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Utils
{
    public class CanvasController : MonoBehaviour
    {
        private GameObject _lastselect;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Update () 
        {         
            if (!EventSystem.current.currentSelectedGameObject)
            {
                EventSystem.current.SetSelectedGameObject(_lastselect);
            }
            else
            {
                _lastselect = EventSystem.current.currentSelectedGameObject;
            }
        }
    }
}
