using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Utils
{
    public class RandomMapGenerator : MonoBehaviour
    {
        [SerializeField] private int x, y;
        
        private Object[] _tiles;
        private Sprite[] _sprites;
        private static bool _created;
        private SpriteRenderer[,] _map;

        private void Awake()
        {
            if(_created)
                Destroy(gameObject);
        }

        private void Start()
        {
            
            InitializeVariables();
            CreateMap();

            SceneManager.sceneLoaded += (arg0, mode) =>
            {
                if (arg0.buildIndex != 0) return;

                RebuildMap();
            };
            
        }

        private void RebuildMap()
        {
            for (var i = 0; i < x; i++)
                for (var j = 0; j < y; j++)
                    _map[i, j].sprite = _sprites[Random.Range(0, _sprites.Length)];
        }

        private void InitializeVariables()
        {
            _tiles = Resources.LoadAll("Env");
            _map = new SpriteRenderer[x, y];
            _sprites = new Sprite[_tiles.Length];

            var a = 0;

            foreach (var t in _tiles)
                _sprites[a++] = ((GameObject) t).GetComponent<SpriteRenderer>().sprite;
            
            DontDestroyOnLoad(gameObject);
            _created = true;
        }

        private void CreateMap()
        {
            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {
                    var obj = (GameObject) Instantiate(_tiles[Random.Range(0, _tiles.Length)], transform, true);
                    obj.transform.localPosition = Vector3.right*i + Vector3.down*j;
                    _map[i, j] = obj.GetComponent<SpriteRenderer>();
                }
            }
        }
        
    }
}
