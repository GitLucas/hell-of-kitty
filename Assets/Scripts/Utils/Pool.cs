﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = System.Random;

namespace Utils
{
    [Serializable]
    public class Pool<T> where T : Component
    {
        private readonly T _prefab;

        [HideInInspector]
        public Func<Component, bool> CanBeUsedCondition = x => !x.gameObject.activeInHierarchy;

        public Pool(T prefab)
        {
            _prefab = prefab;
        }

        public List<T> PooledObjects { get; protected set; } = new List<T>();

        public T Get()
        {
            foreach (var o in PooledObjects.Where(o => CanBeUsedCondition(o)))
                return o;
            
            return CreateNew();
        }

        private T CreateNew()
        {
            var newObject = Object.Instantiate(_prefab);
            newObject.gameObject.SetActive(false);
            PooledObjects.Add(newObject);
            return newObject;
        }
    }
}