using UnityEngine;

public class PlayerSpriteSelector : MonoBehaviour
{
    [SerializeField] private Sprite[] cats;

    [SerializeField] private SpriteRenderer myRenderer;
    
    
    private void Start()
    {
        myRenderer.sprite = cats[PlayerPrefs.GetInt("CatIndex")];
    }
}
