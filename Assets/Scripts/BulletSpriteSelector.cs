using UnityEngine;
using Random = UnityEngine.Random;

public class BulletSpriteSelector : MonoBehaviour
{
    [SerializeField] private SpriteRenderer myRenderer;
    [SerializeField] private Sprite[] sprites;
    
    private void OnEnable()
    {
        myRenderer ??= GetComponent<SpriteRenderer>();
        myRenderer.sprite = sprites[Random.Range(0, sprites.Length)];
    }
}
