using System;
using Global;
using UnityEngine;

public class CrossSceneObjectManager : MonoBehaviour
{
    private static CrossSceneObjectManager _instance;

    public static CrossSceneObjectManager Instance
    {
        get
        {
            if (!_instance)
                _instance = FindObjectOfType<CrossSceneObjectManager>();

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance)
        {
            Destroy(gameObject);
            return;
        }
        
        _instance = this;
    }

    private SceneController _scenes;
    
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        
        _scenes = new SceneController();
    }

    public void GoToGame() => _scenes.GoToGame();

}
