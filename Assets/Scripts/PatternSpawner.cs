using System;
using UnityEngine;

public class PatternSpawner : MonoBehaviour
{
    [SerializeField] private BulletSpawner[] spawners;
    private int _spawnersComplete;

    private void Awake()
    {
        if (spawners.Length < 1)
            spawners = GetComponentsInChildren<BulletSpawner>();
    }

    private void Start()
    {
        Deactivate();
    }

    private void HandleSpawnComplete(BulletSpawner spawner)
    {
        spawner.SpawnComplete -= HandleSpawnComplete;
        
        if (++_spawnersComplete < spawners.Length) return;

        try
        {
            Invoke(nameof(Deactivate), 4f);
        }
        catch (MissingReferenceException)
        {
        }
    }
    
    private void Deactivate() => gameObject.SetActive(false);

    public void Activate()
    {
        gameObject.SetActive(true);
        
        _spawnersComplete = 0;
        
        foreach (var spawner in spawners)
        {
            spawner.SpawnComplete += HandleSpawnComplete;
            spawner.Spawn();
        }
    }
}
