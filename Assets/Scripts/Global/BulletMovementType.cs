public enum BulletMovementType
{
    SineUp,
    SineDown,
    SineLeft,
    SineRight,
    SpiralLeft,
    SpiralRight,
    Right,
    Left,
    RightUp,
    RightDown,
    LeftUp,
    LeftDown,
    Up,
    Down
}
