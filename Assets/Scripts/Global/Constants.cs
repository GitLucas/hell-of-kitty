using UnityEngine;

namespace Global
{
    public static class Constants
    {
        public const int TopLimit    = 23;
        public const int BottomLimit = -23;
        public const int LeftLimit   = -31;
        public const int RightLimit  = 31;

        public static readonly float BulletMovementSpeed = 10f * Time.fixedDeltaTime;
        public static readonly float BulletRotationRadiusIncrement = 5f * Time.fixedDeltaTime;
    }
}
