using UnityEngine.SceneManagement;

namespace Global
{
    public class SceneController 
    {
        public void GoToGame()
        {
            SceneManager.LoadScene(1);
            GameEventsHandler.GameLoss += BackToMenu;
        }

        private void BackToMenu()
        {
            GameEventsHandler.GameLoss -= BackToMenu;
            SceneManager.LoadScene(0);
        }
    }
}
