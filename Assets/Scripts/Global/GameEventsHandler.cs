using System;

namespace Global
{
    public class GameEventsHandler
    {
        private static GameEventsHandler _instance;

        public static GameEventsHandler Instance => _instance ??= new GameEventsHandler();

        public static event Action GameLoss;
        public static event Action CoinGrabbed;

        private GameEventsHandler() { }

        public void InvokeGameLost() => GameLoss?.Invoke();
        public void InvokeCoinGrabbed() => CoinGrabbed?.Invoke();
    }
}
