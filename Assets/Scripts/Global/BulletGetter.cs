using System;
using BulletPatterns;
using UnityEngine;
using Utils;

namespace Global
{
    public class BulletGetter : MonoBehaviour
    {
        
        #region Singleton

        private static BulletGetter _instance;

        public static BulletGetter Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<BulletGetter>();

                return _instance;
            }
        }

        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            
            _instance = this;
            Initialize();
        }

        #endregion

        private Pool<Transform> _bullets;
        /*private Pool<SineLeft>    _sineLeftPool;
        private Pool<SineRight>   _sineRightPool;
        private Pool<SineDown>    _sineDownPool;
        private Pool<SineUp>      _sineUpPool;
        private Pool<SpiralLeft>  _spiralLeftPool;
        private Pool<SpiralRight> _spiralRightPool;*/

        private void Initialize()
        {
            _bullets = new Pool<Transform>(Resources.Load<Transform>("Bullets/Empty"));
            /*_sineLeftPool = new Pool<SineLeft>(Resources.Load<SineLeft>("Bullets/SineLeft"));
            _sineRightPool = new Pool<SineRight>(Resources.Load<SineRight>("Bullets/SineRight"));
            _sineDownPool = new Pool<SineDown>(Resources.Load<SineDown>("Bullets/SineDown"));
            _sineUpPool = new Pool<SineUp>(Resources.Load<SineUp>("Bullets/SineUp"));
            _spiralLeftPool = new Pool<SpiralLeft>(Resources.Load<SpiralLeft>("Bullets/SpiralLeft"));
            _spiralRightPool = new Pool<SpiralRight>(Resources.Load<SpiralRight>("Bullets/SpiralRight"));*/
        }

        public GameObject GetBullet(BulletMovementType type)
        {
            var bullet = _bullets.Get().gameObject;

            switch (type)
            {
                case BulletMovementType.SineUp:
                    bullet.AddComponent<SineUp>();
                    break;
                case BulletMovementType.SineDown:
                    bullet.AddComponent<SineDown>();
                    break;
                case BulletMovementType.SineLeft:
                    bullet.AddComponent<SineLeft>();
                    break;
                case BulletMovementType.SineRight:
                    bullet.AddComponent<SineRight>();
                    break;
                case BulletMovementType.SpiralLeft:
                    bullet.AddComponent<SineLeft>();
                    break;
                case BulletMovementType.SpiralRight:
                    bullet.AddComponent<SineRight>();
                    break;
                case BulletMovementType.Right:
                    bullet.AddComponent<Linear>().SetDirections(1, 0);
                    break;
                case BulletMovementType.Left:
                    bullet.AddComponent<Linear>().SetDirections(-1, 0);
                    break;
                case BulletMovementType.RightUp:
                    bullet.AddComponent<Linear>().SetDirections(1, 1);
                    break;
                case BulletMovementType.RightDown:
                    bullet.AddComponent<Linear>().SetDirections(1, -1);
                    break;
                case BulletMovementType.LeftUp:
                    bullet.AddComponent<Linear>().SetDirections(-1, 1);
                    break;
                case BulletMovementType.LeftDown:
                    bullet.AddComponent<Linear>().SetDirections(-1, -1);
                    break;
                case BulletMovementType.Up:
                    bullet.AddComponent<Linear>().SetDirections(0, 1);
                    break;
                case BulletMovementType.Down:
                    bullet.AddComponent<Linear>().SetDirections(0, -1);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            };

            return bullet;
        }
        
    }
}
