using Global;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player")) return;
        
        GameEventsHandler.Instance.InvokeCoinGrabbed();
        gameObject.SetActive(false);
    }
}
